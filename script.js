'use strict'

// 1. Додати новий абзац по кліку на кнопку:
//  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p>
//  з текстом "New Paragraph" і додайте його до розділу <section id="content">

const clickMeButton = document.querySelector('#btn-click');

clickMeButton.addEventListener('click', onClickAddParagraph);

function onClickAddParagraph() {
    const newParagraph = document.createElement('p');
    newParagraph.textContent = 'New Paragraph';
    clickMeButton.before(newParagraph);
}

// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні
//  атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const footer = document.querySelector('footer');
const sectionForm = document.createElement('section');
sectionForm.className = 'content';
footer.before(sectionForm);

const btnInputCreate = document.createElement('button');
btnInputCreate.className = 'btn-input-create btn-click';
btnInputCreate.textContent = 'Add new input';
sectionForm.append(btnInputCreate);

btnInputCreate.addEventListener('click', onClickAddInput);

function onClickAddInput() {
    const newInput = document.createElement('input');
    newInput.setAttribute('type', 'text');
    newInput.name = 'clientName';
    newInput.placeholder = 'Enter name';
    newInput.id = 'input-text';
    btnInputCreate.after(newInput);
}

